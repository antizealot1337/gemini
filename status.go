package gemini

// Status is a numeric code that the server sends to inform the client the
// nature of the response.
type Status uint8

const (
	// StatusInput means the server is requesting input from the client.
	StatusInput Status = 10

	// StatusInputSensitive is the same as StatusInput but the server is
	// requesting sensitive information (like a password) that the client might
	// want to hide.
	StatusInputSensitive Status = 11

	// StatusSuccess means the request was successful.
	StatusSuccess Status = 20

	// StatusTemporaryRedirect means the resource has moved to a new URL but not
	// permanently.
	StatusTemporaryRedirect Status = 30

	// StatusPermanentRedirect means the resource has moved permanently and
	// future requests should be made to the new URL.
	StatusPermanentRedirect Status = 31

	// StatusTemporaryFailure ...
	StatusTemporaryFailure Status = 40

	// StatusServerUnavailable means the server is unavailable due to overload
	// or maintenance.
	StatusServerUnavailable Status = 41

	// StatusCGIError means the cgi process either erred or timed out.
	StatusCGIError Status = 42

	// StatusProxyError means the the proxy request failed because the server
	// was unable to establish a connection.
	StatusProxyError Status = 43

	// StatusSlowDown means rate limiting is in effect. The server should also
	// send an integer with the number of seconds a client should wait before
	// repeating the request.
	StatusSlowDown Status = 44

	// StatusPermanentFailure ...
	StatusPermanentFailure Status = 50

	// StatusNotFound means the resource could not be found but may be available
	// in the future.
	StatusNotFound Status = 51

	// StatusGone means the resource is no longer available and will not be
	// again. And crawlers or search engines should remove the resource from
	// their listings.
	StatusGone Status = 52

	// StatusProxyRequestRefused means the resource was at a domain not served
	// by the server and that server does not accept proxy requests.
	StatusProxyRequestRefused Status = 53

	// StatusBadRequest means the server was unable to parse the client's
	// requests presumably because it was malformed.
	StatusBadRequest Status = 59

	// StatusClientCertificateRequired means to access a particular resource a
	// client certificate is required.
	StatusClientCertificateRequired Status = 60

	// StatusClientCertificateNotAuthorized means the client certificate is not
	// authorized to access the required resource.
	StatusClientCertificateNotAuthorized Status = 61

	// StatusClientCertificateNotValid means the supplied client certificate was
	// no accepted because it is not valid.
	StatusClientCertificateNotValid Status = 62
)

func (s Status) String() string {
	switch s {
	case StatusInput:
		return "Input"
	case StatusInputSensitive:
		return "Sensitive Input"
	case StatusSuccess:
		return "Success"
	case StatusTemporaryRedirect:
		return "Temporary Redirect"
	case StatusPermanentRedirect:
		return "Permanent Redirect"
	case StatusTemporaryFailure:
		return "Temporary Failure"
	case StatusServerUnavailable:
		return "Server Unavailable"
	case StatusCGIError:
		return "CGI Error"
	case StatusProxyError:
		return "Proxy Error"
	case StatusSlowDown:
		return "Slow Down"
	case StatusPermanentFailure:
		return "Permanent Failure"
	case StatusNotFound:
		return "Not Found"
	case StatusGone:
		return "Gone"
	case StatusProxyRequestRefused:
		return "Proxy Request Refused"
	case StatusBadRequest:
		return "Bad Request"
	case StatusClientCertificateRequired:
		return "Client Certificate Required"
	case StatusClientCertificateNotAuthorized:
		return "Client Certificate Not Authorized"
	case StatusClientCertificateNotValid:
		return "Client Certificate Not Valid"
	default:
		return "unknown status code"
	} //switch
} //func
