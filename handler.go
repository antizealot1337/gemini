package gemini

// Handler represents a connection handler.
type Handler interface {
	Serve(rw ResponseWriter, r *Request)
} //interface

// HandleFunc is a func that implements Handler.
type HandleFunc func(ResponseWriter, *Request)

// Serve allows HandleFunc to implement Handler.
func (h HandleFunc) Serve(rw ResponseWriter, r *Request) {
	h(rw, r)
} //func
