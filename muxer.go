package gemini

import "strings"

// Muxer is a group of Handlers that are called based on the Request.URL.Path.
type Muxer struct {
	handlers map[string]Handler
} //struct

// NewMuxer creates a new Muxer.
func NewMuxer() *Muxer {
	return &Muxer{
		handlers: map[string]Handler{},
	}
} //func

// Handle will add the Handler to the Muxer. Any path that begins with address
// will be called. If 2 paths match the longest one wins.
func (m *Muxer) Handle(address string, handler Handler) {
	m.handlers[address] = handler
} //func

// HandleFunc is just like Handle except a HandleFunc can be passed.
func (m *Muxer) HandleFunc(address string, handler HandleFunc) {
	m.Handle(address, handler)
} //func

// Serve will look at the Request.URL.Path and try to find an appropriate
// Handler to serve the request. If none is found this will panic.
func (m *Muxer) Serve(rw ResponseWriter, r *Request) {
	var a string
	var h Handler

	for addr, handler := range m.handlers {
		if strings.HasPrefix(r.URL.Path, addr) && len(addr) > len(a) {
			a = addr
			h = handler
		} //if
	} //if

	h.Serve(rw, r)
} //func
