package gemini

import (
	"net"
	"net/url"
)

// Request represents a request for a resource.
type Request struct {
	URL  *url.URL
	Addr net.Addr
} //struct

// NewRequest creates a new Request. The resource should be a valid URL.
func NewRequest(resource string) (*Request, error) {
	u, err := url.Parse(resource)
	if err != nil {
		return nil, err
	} //if

	return &Request{
		URL: u,
	}, nil
} //func
