package gemini

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/url"
	"strings"
)

// Client is a reusable client for connecting to gemini:// servers.
type Client struct {
	TLSConfig *tls.Config
} //struct

// Do instructs the Client to perform the Request.
func (c *Client) Do(r *Request) (*Response, error) {
	conn, err := net.Dial("tcp", getHost(r.URL))
	if err != nil {
		return nil, err
	} //if

	if c.TLSConfig != nil {
		conn = tls.Client(conn, c.TLSConfig)
	} //if

	fmt.Fprintf(conn, "%s\r\n", r.URL.String())

	resp, err := parseResponse(conn)
	if err != nil {
		return nil, err
	} //if

	switch c := conn.(type) {
	case *tls.Conn:
		resp.ConnState = c.ConnectionState()
	} //switch

	return resp, nil
} //func

// Request is an easier way to request a resource. It constructs a Request and
// calls Client.Do on said Request.
func (c *Client) Request(resource string) (*Response, error) {
	r, err := NewRequest(resource)
	if err != nil {
		return nil, err
	} //if
	return c.Do(r)
} //func

func getHost(u *url.URL) string {
	if strings.Contains(u.Host, ":") {
		return u.Host
	} //if

	return u.Host + ":1965"
} //if
