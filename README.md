# Gemini
This is a package that implements the basics needed for gemini clients and
servers https://gemini.circumlunar.space/.

## APIs
The API of this project is heavily inspired by `net/http` in the Go standard
library. Some things have not been implemented yet and others will probably not
be implemented due to either current conventions or irrelevance to gemini.
The API is still in flux and may change in the future.

The client and server in this package have the non standards-compliant ability
to connect via raw sockets (no TLS). This should only be used for testing or
maybe in some proxy settings. DO NOT use this capability for real gemini
servers.

## Client
The following code is an example of the Client. It doesn't do any certificate
verification and should not be used for production software.

```go
package main

import (
	"crypto/tls"
	"fmt"
	"io"
	"os"

	"bitbucket.org/antizealot1337/gemini"
)

func main() {
	c := gemini.Client{
		TLSConfig: &tls.Config{
			MinVersion:         tls.VersionTLS12,
			InsecureSkipVerify: true,
		},
	}

	resloc := os.Args[1]

	if resloc == "" {
        fmt.Println("usage:", os.Args[0], "URL")
		return
	} //if

	resp, err := c.Request(resloc)
	if err != nil {
		panic(err)
	} //if
	defer resp.Body.Close()

	fmt.Println(resp.Status, resp.Meta)

	_, err = io.Copy(os.Stdout, resp.Body)
	if err != nil {
		panic(err)
	} //if
} //func
```

## Server
The following is an example Server. The Server needs the certificates to start.
Without them this example will fail. Certificates can be generated via OpenSSL
via `openssl req -x509 -nodes -newkey rsa:2048 -keyout server.rsa.key -out server.rsa.crt -days 3650`.

```go
package main

import (
	"crypto/tls"
	"fmt"
	"log"

	"bitbucket.org/antizealot1337/gemini"
)

var (
	addr = ":1965"
)

func main() {
	c, err := tls.LoadX509KeyPair("server.rsa.crt", "server.rsa.key")
	if err != nil {
		panic(err)
	} //if

	tlsconfig := &tls.Config{
		Certificates: []tls.Certificate{c},
	}

	muxer := gemini.NewMuxer()

	muxer.HandleFunc("/", func(rw gemini.ResponseWriter, r *gemini.Request) {
		rw.WriteStatus(gemini.StatusSuccess, "text/plain")
		fmt.Fprint(rw, "Hello, World!")
		log.Println(r.Addr, r.URL.Path, gemini.StatusSuccess)
	})

	muxer.HandleFunc("/greet", func(rw gemini.ResponseWriter, r *gemini.Request) {
		vals := r.URL.Query()

		name := vals.Get("name")

		if name == "" {
			rw.WriteStatus(gemini.StatusTemporaryRedirect, "/")
			log.Println(r.Addr, r.URL.Path, gemini.StatusSuccess)
			return
		} //if

		rw.WriteStatus(gemini.StatusSuccess, "text/plain")
		fmt.Fprintf(rw, "Hello, %s!", name)
	})

	srv := gemini.Server{
		TLSConfig: tlsconfig,
	}

	log.Println("Listening on", addr)

	if err := srv.Listen(addr, muxer); err != nil {
		log.Fatal(err)
	} //if
} //func
```

## Copyright
Licensed under the terms of the MIT License. Please see LICENSE file for more
information.