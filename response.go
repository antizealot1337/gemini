package gemini

import (
	"crypto/tls"
	"fmt"
	"io"
)

// Response represents the response to a Client Request.
type Response struct {
	Status    Status
	Meta      string
	Body      io.ReadCloser
	ConnState tls.ConnectionState
} //struct

func parseResponse(r io.ReadCloser) (*Response, error) {
	resp := Response{
		Body: r,
	}

	_, err := fmt.Fscanf(r, "%d %s\r\n", &resp.Status, &resp.Meta)
	if err != nil {
		return nil, err
	} //if

	return &resp, nil
} //func
