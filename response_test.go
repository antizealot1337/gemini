package gemini

import (
	"io/ioutil"
	"strings"
	"testing"
)

func Test_parseResponse(t *testing.T) {
	resp, err := parseResponse(ioutil.NopCloser(strings.NewReader("20 text/plain \r\nthis is plain text")))
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if
	defer resp.Body.Close()

	if expected, actual := StatusSuccess, resp.Status; actual != expected {
		t.Errorf("expected status %d but ws %d", expected, actual)
	} //if

	if expected, actual := "text/plain", resp.Meta; actual != expected {
		t.Errorf(`expected meta "%s" but was "%s'`, expected, actual)
	} //if

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := "this is plain text", string(data); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func
