package gemini

import (
	"crypto/tls"
	"fmt"
	"net"
)

// Server manages connections.
type Server struct {
	TLSConfig *tls.Config
	running   bool
} //struct

// Listen for incoming connections on the provided address. If the tls.Config is
// nil then the Server will not use TLS. This should only be used when the
// server is behind a proxy handling TLS or for testing since clients expect to
// connect via TLS.
func (s *Server) Listen(address string, handler Handler) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	} //if

	if s.TLSConfig != nil {
		listener = tls.NewListener(listener, s.TLSConfig)
	} //if

	defer listener.Close()

	s.running = true

	for s.running {
		conn, err := listener.Accept()
		if err != nil {
			return err
		} //if

		go handleConnection(conn, handler)
	} //for

	return nil
} //func

// Stop will inform the Server to stop receiving incoming connections.
func (s *Server) Stop() {
	s.running = false
} //func

func handleConnection(conn net.Conn, handler Handler) {
	var resource string

	_, err := fmt.Fscanf(conn, "%s\r\n", &resource)
	if err != nil {
		return
	} //if

	r, err := NewRequest(resource)
	if err != nil {
		return
	} //if

	r.Addr = conn.RemoteAddr()

	rw := responseWriter{
		out: conn,
	}

	defer conn.Close()

	handler.Serve(&rw, r)
} //func
