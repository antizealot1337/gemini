package gemini

import (
	"fmt"
	"io"
)

// ResponseWriter writes the a response to a Client connection.
type ResponseWriter interface {
	io.Writer
	WriteStatus(code Status, meta string) error
} //interface

type responseWriter struct {
	out           io.Writer
	statusWritten bool
} //struct

func (r *responseWriter) Write(b []byte) (int, error) {
	if !r.statusWritten {
		err := r.WriteStatus(StatusSuccess, "application/octet-stream")
		if err != nil {
			return 0, err
		} //if
	}
	return r.out.Write(b)
} //func

func (r *responseWriter) WriteStatus(code Status, meta string) error {
	_, err := fmt.Fprintf(r.out, "%d %s\r\n", int(code), meta)
	if err != nil {
		return err
	} //if

	r.statusWritten = true

	return nil
} //func
