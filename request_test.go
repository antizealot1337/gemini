package gemini

import "testing"

func TestNewRequest(t *testing.T) {
	r, err := NewRequest("gemini://example.org/path/to/resource")
	if err != nil {
		t.Error("unexpected error:", err)
	} //if

	if r.Addr != nil {
		t.Error("expected Addr to be nil")
	} //if

	if r.URL == nil {
		t.Fatal("expected URL to not be nil")
	} //if
} //func
