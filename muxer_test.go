package gemini

import (
	"net/url"
	"testing"
)

var _ Handler = (*Muxer)(nil)

func TestNewMuxer(t *testing.T) {
	m := NewMuxer()

	if m == nil {
		t.Fatal("did not expect NewMuxer to return nil")
	} //if

	if m.handlers == nil {
		t.Error("expected handler to not be nil")
	} //if
} //func

func TestMuxerHandle(t *testing.T) {
	m := NewMuxer()
	h := NewMuxer() // Use this for a Handler

	m.Handle("/", h)

	if expected, actual := 1, len(m.handlers); actual != expected {
		t.Errorf("expected %d handlers but was %d", expected, actual)
	} //if
} //func

func TestMuxerHandleFunc(t *testing.T) {
	m := NewMuxer()

	m.HandleFunc("/", func(rw ResponseWriter, r *Request) {

	})

	if expected, actual := 1, len(m.handlers); actual != expected {
		t.Errorf("expected %d handlers but was %d", expected, actual)
	} //if
} //func

func TestMuxerServe(t *testing.T) {
	m := NewMuxer()

	a := false

	m.HandleFunc("/a", func(rw ResponseWriter, r *Request) {
		a = true
	})

	b := false

	m.HandleFunc("/b", func(rw ResponseWriter, r *Request) {
		b = true
	})

	r := Request{
		URL: &url.URL{
			Path: "/a",
		},
	}

	m.Serve(nil, &r)

	if !a {
		t.Error("muxer did not correctly call /a handler")
	} //if

	r.URL.Path = "/b/bob"
	m.Serve(nil, &r)

	if !b {
		t.Error("muxer did not correctly call /b handler")
	} //if
} //func
