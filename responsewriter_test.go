package gemini

import (
	"bytes"
	"testing"
)

var _ ResponseWriter = (*responseWriter)(nil)

func Test_responseWriterWrite(t *testing.T) {
	t.Run("TestNoWriteStatus", func(t *testing.T) {
		var buf bytes.Buffer

		rw := responseWriter{
			out: &buf,
		}

		_, err := rw.Write([]byte("1"))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if !rw.statusWritten {
			t.Error("expected statusWritten to be set")
		} //if

		if expected, actual := "20 application/octet-stream\r\n1", buf.String(); actual != expected {
			t.Errorf(`expected "%s" to be "%s"`, expected, actual)
		} //if
	})
	t.Run("TestWriteWithWriteStatus", func(t *testing.T) {
		var buf bytes.Buffer

		rw := responseWriter{
			out: &buf,
		}

		err := rw.WriteStatus(StatusSuccess, "text/plain")
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		_, err = rw.Write([]byte("1"))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if !rw.statusWritten {
			t.Error("expected statusWritten to be set")
		} //if

		if expected, actual := "20 text/plain\r\n1", buf.String(); actual != expected {
			t.Errorf(`expected "%s" to be "%s"`, expected, actual)
		} //if
	})
} //func

func Test_responseWriterWriteStatus(t *testing.T) {
	var buf bytes.Buffer

	rw := responseWriter{
		out: &buf,
	}

	err := rw.WriteStatus(StatusSuccess, "text/plain")
	if err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := "20 text/plain\r\n", buf.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if

	if !rw.statusWritten {
		t.Error("expected statusWritten to be set")
	} //if
} //func
