package gemini

import (
	"net/url"
	"testing"
)

func Test_getHost(t *testing.T) {
	t.Run("TestWithPort", func(t *testing.T) {
		u, err := url.Parse("schema://localhost:4/path/to/something")
		if err != nil {
			t.Error("unexpected error:", err)
		} //if
		if expected, actual := "localhost:4", getHost(u); actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestWithoutPort", func(t *testing.T) {
		u, err := url.Parse("schema://localhost/path/to/something")
		if err != nil {
			t.Error("unexpected error:", err)
		} //if
		if expected, actual := "localhost:1965", getHost(u); actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
} //func
