package gemini

import "testing"

func TestStatusString(t *testing.T) {
	checkStatus := func(s Status, m string) {
		if expected, actual := m, s.String(); actual != expected {
			t.Errorf(`expected "%s" from status %d but was "%s"`, expected, s,
				actual)
		} //if
	} //func

	checkStatus(0, "unknown status code")
	checkStatus(StatusInput, "Input")
	checkStatus(StatusInputSensitive, "Sensitive Input")
	checkStatus(StatusSuccess, "Success")
	checkStatus(StatusTemporaryRedirect, "Temporary Redirect")
	checkStatus(StatusPermanentRedirect, "Permanent Redirect")
	checkStatus(StatusTemporaryFailure, "Temporary Failure")
	checkStatus(StatusServerUnavailable, "Server Unavailable")
	checkStatus(StatusCGIError, "CGI Error")
	checkStatus(StatusProxyError, "Proxy Error")
	checkStatus(StatusSlowDown, "Slow Down")
	checkStatus(StatusPermanentFailure, "Permanent Failure")
	checkStatus(StatusNotFound, "Not Found")
	checkStatus(StatusGone, "Gone")
	checkStatus(StatusProxyRequestRefused, "Proxy Request Refused")
	checkStatus(StatusBadRequest, "Bad Request")
	checkStatus(StatusClientCertificateRequired, "Client Certificate Required")
	checkStatus(StatusClientCertificateNotAuthorized,
		"Client Certificate Not Authorized")
	checkStatus(StatusClientCertificateNotValid, "Client Certificate Not Valid")
} //func
