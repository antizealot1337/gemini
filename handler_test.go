package gemini

import "testing"

var _ Handler = (*HandleFunc)(nil)

func TestHandleFuncHandle(t *testing.T) {
	called := false
	var handle HandleFunc = func(rw ResponseWriter, r *Request) {
		called = true
		if rw == nil {
			t.Error("expected rw to not be nil")
		} //if
		if r == nil {
			t.Error("expected r to not be nil")
		} //if
	}

	handle.Serve(&responseWriter{}, &Request{})

	if !called {
		t.Error("expected Handle to call the anonymouse function")
	} //if
} //func
